# "Manage search engines" button #

## Summary ##

Easily reach the "Manage Search Engines" dialog using a toolbar icon!

 * Open the search engine dialog without using the search bar
 * Access the search engine menu by clicking on the panel button
   (can also be changed in the settings to show the dialog directly)
 * Panel button highlighting when new search engines are available
   (can be disabled)

## Adding translations ##
Interested in adding a translation? Please
[download the template file](https://gitlab.com/alexander255/manage-search-engines/raw/master/locale/template.properties),
follow its instructions and submit the result as a merge request or send me an
e-mail to alexander-amo@xmine128.tk with the translated file! Thank you!

## Links ##

 * [Add-on listing on addons.mozilla.org](https://addons.mozilla.org/de/firefox/addon/manage-search-engines-button/)
 * [Version archive and Downloads](https://addons.mozilla.org/firefox/addon/manage-search-engines-button/versions/)
 * [Source code repository](https://gitlab.com/alexander255/manage-search-engines/)