const sdk = {
	l10n:        require("sdk/l10n"),
	panel:       require("sdk/panel"),
	simpleprefs: require("sdk/simple-prefs"),
	system:      require("sdk/system")
};

const m = {
	handlers: {
		button: require("./handlers/button")
	}
};

const _ = sdk.l10n.get;


var handler = new m.handlers.button.Handler();



// Setup button highlighter
var EngineMonitor = require("./monitor").EngineMonitor;

// Create highlighting monitor
var monitor = new EngineMonitor(function(window)
{
	handler.setHighlight(window, true);
}, function(window)
{
	handler.setHighlight(window, false);
});

function toggle_monitor()
{
	// Start or stop monitoring based on pref
	if(sdk.simpleprefs.prefs["display-addengine-notification"]) {
		monitor.init();
	} else {
		monitor.uninit();
	}
}

// Start highlighter if it is supposed to run
toggle_monitor();

// Change on pref change
sdk.simpleprefs.on("display-addengine-notification", toggle_monitor);