const sdk = {
	tabs:    require("sdk/tabs"),
	windows: require("sdk/windows"),
	
	system: {
		unload: require("sdk/system/unload")
	},
	view: {
		core: require("sdk/view/core")
	}
};

const tools = {
	searchengines: require("./tools/search-engines")
};



function EngineMonitor(callbackHighlight, callbackUnhighlight)
{
	this.callbackHighlight   = callbackHighlight;
	this.callbackUnhighlight = callbackUnhighlight;
	
	this.onOpenHandler = this.onOpen.bind(this);
	this.updateHandler = this.update.bind(this);
	
	this.onEngineEventHandler = this.onEngineEvent.bind(this);
	
	sdk.system.unload.when(function()
	{
		this.uninit("system");
	}.bind(this));
	
	this.isRunning = false;
}
EngineMonitor.prototype = {
	init: function()
	{
		if(this.isRunning) {
			return;
		}
		
		sdk.tabs.on("open", this.onOpenHandler);
		
		tools.searchengines.on("engine-added",     this.onEngineEventHandler);
		tools.searchengines.on("engine-available", this.onEngineEventHandler);
		
		for(var tab of sdk.tabs) {
			this.onOpen(tab);
		}
		
		this.isRunning = true;
	},
	
	uninit: function(reason)
	{
		if(!this.isRunning) {
			return;
		}
		
		sdk.tabs.removeListener("open", this.onOpenHandler);
		
		// Unsubscribe from tab events
		for(var tab of sdk.tabs) {
			tab.removeListener("activate", this.updateHandler);
			tab.removeListener("ready",    this.updateHandler);
		}
		
		// Make sure don't stay highlighted
		if(reason !== "system") {
			for(var window of sdk.windows.browserWindows) {
				this.callbackUnhighlight(window);
			}
		}
		
		this.isRunning = false;
	},
	
	update: function(tab)
	{
		tools.searchengines.waitUntilReady(function() {
			if(tab === tab.window.tabs.activeTab) {
				if(tools.searchengines.getOfferedEngines(tab).length > 0) {
					this.callbackHighlight(tab.window);
				} else {
					this.callbackUnhighlight(tab.window);
				}
			}
		}.bind(this));
	},
	
	onOpen: function(tab)
	{
		// Page changed
		this.update(tab);
		
		// Subscribe to tab events
		tab.on("activate", this.updateHandler);
		tab.on("ready",    this.updateHandler);
	},
	
	onEngineEvent: function()
	{
		// Engine list possibly changed
		this.update(sdk.tabs.activeTab);
	}
};

module.exports = {
	EngineMonitor: EngineMonitor
};
