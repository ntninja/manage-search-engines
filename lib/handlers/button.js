const sdk = {
	l10n:        require("sdk/l10n"),
	simpleprefs: require("sdk/simple-prefs"),
	ui:          require("sdk/ui"),
	
	system: {
		unload: require("sdk/system/unload")
	}
};

const tools = {
	ui: {
		button: {
			highlight: require("../tools/ui/button/highlight")
		}
	}
};

const m = {
	ui: {
		manager: require("../ui/manager"),
		menu:    require("../ui/menu")
	}
};

const _ = sdk.l10n.get;



function Handler()
{
	sdk.system.unload.when(this.uninit.bind(this));
	
	this.init();
}
Handler.prototype = {
	init: function()
	{
		if(typeof(this.button) !== "object") {
			this.button = sdk.ui.ToggleButton({
				id:    "manage-search-engines",
				label: _("Manage search engines"),
				icon: {
					"16": "./icon-16.png",
					"32": "./icon-32.png",
					"64": "./icon-64.png"
				},
				
				onChange: function(state)
				{
					if(state.checked) {
						if(sdk.simpleprefs.prefs["display-menupopup"]) {
							// Open the list of search engines as a menupopup
							// (not a shitty SDK panel...)
							m.ui.menu.show(
								{
									position: this.button
								},
								
								function()
								{
									this.button.state("window", {
										checked: false
									});
								}.bind(this)
							);
						} else {
							// Open engine manager
							m.ui.manager.open();
							
							// Reset toggle button state
							this.button.state("window", {
								checked: false
							});
						}
					}
				}.bind(this)
			});
		}
	},
	
	setHighlight: function(window, highlighted)
	{
		// Apply highlighting
		if(highlighted) {
			tools.ui.button.highlight.enable(this.button, [window]);
		} else {
			tools.ui.button.highlight.disable(this.button, [window]);
		}
	},
	
	uninit: function()
	{
		if(typeof(this.button) === "object") {
			this.button.destroy();
			delete this.button;
		}
	}
};

module.exports = {
	Handler: Handler
};
