"use strict";

const sdk = {
	url: require("sdk/url"),
	
	core: {
		namespace: require("sdk/core/namespace")
	},
	system: {
		unload: require("sdk/system/unload")
	},
	view: {
		core: require("sdk/view/core")
	}
};

const {Ci, Cu, Cr} = require("chrome");
Cu.import("resource://gre/modules/XPCOMUtils.jsm");



var internal = sdk.core.namespace.ns();



var TabProgressListener = function(tab_model, callbacks)
{
	if(!(this instanceof TabProgressListener)) {
		return new TabProgressListener(tab_model, callbacks);
	}
	
	// Remember tab model
	internal(this).tabModel = tab_model;
	
	// Use the low-level tab interface
	internal(this).webProgress = sdk.view.core.viewFor(tab_model).linkedBrowser.webProgress;
	internal(this).callbacks   = callbacks;
	
	// Make sure all callbacks have an appropriate context
	if(typeof(internal(this).callbacks.context) !== "object") {
		internal(this).callbacks.context = internal(this).callbacks;
	}
	
	// Start listening
	internal(this).webProgress.addProgressListener(this, this.eventMask);
	
	// Handle unload cleanly
	sdk.system.unload.ensure(this, "destroy");
};
TabProgressListener.prototype = {
	QueryInterface: XPCOMUtils.generateQI(
			["nsIWebProgressListener", "nsIWebProgressListener2", "nsISupportsWeakReference"]
	),
	
	
	/**
	 * Event mask used when registering the progress listener
	 *
	 * Only events that have a registered handler in {@see callbacks}
	 * will be set in the returned bit mask.
	 *
	 * @return {Number}
	 */
	get eventMask()
	{
		var mask = 0;
		
		if(typeof(internal(this).callbacks.onLocationChange) === "function") {
			mask |= internal(this).webProgress.NOTIFY_LOCATION;
		}
		
		if(typeof(internal(this).callbacks.onProgressChange) === "function") {
			mask |= internal(this).webProgress.NOTIFY_PROGRESS;
		}
		
		if(typeof(internal(this).callbacks.onRefreshAttempted) === "function") {
			mask |= internal(this).webProgress.NOTIFY_REFRESH;
		}
		
		if(typeof(internal(this).callbacks.onSecurityChange) === "function") {
			mask |= internal(this).webProgress.NOTIFY_SECURITY;
		}
		
		if(typeof(internal(this).callbacks.onStateChange) === "function") {
			mask |= internal(this).webProgress.NOTIFY_STATE_ALL;
		}
		
		if(typeof(internal(this).callbacks.onStatusChange) === "function") {
			mask |= internal(this).webProgress.NOTIFY_STATUS;
		}
		
		return mask;
	},
	
	
	get tab()
	{
		return internal(this).tabModel;
	},
	
	
	/**
	 * Tear down web progress listener for this tab
	 */
	destroy: function()
	{
		// Stop listening
		internal(this).webProgress.removeProgressListener(this);
		
		// Release reference on web progress but keep the one on the callback since this object currently cann't
		// handle a callback less state...
		//TODO: Make it possible to relase the callbacks list as well
		delete internal(this).tabModel;
		delete internal(this).webProgress;
	},
	
	
	/**
	 * @internal
	 */
	onLocationChange: function(progress, request, URI, flagMask)
	{
		if(typeof(internal(this).callbacks.onLocationChange) === "function") {
			// Create list of flags
			var flags = [];
			if((flagMask | Ci.nsIWebProgressListener.LOCATION_CHANGE_SAME_DOCUMENT) === flagMask) {
				flags.push("same-document");
			}
			if((flagMask | Ci.nsIWebProgressListener.LOCATION_CHANGE_ERROR_PAGE) === flagMask) {
				flags.push("error-page");
			}
			
			// Call callback
			internal(this).callbacks.onLocationChange.call(
					internal(this).callbacks.context,
					this,
					request,
					sdk.url.URL(URI.spec),
					flags
			);
		}
	},
	
	
	/**
	 * @internal
	 */
	onProgressChange64: function(progress, request, progressSelf, progressSelfMax, progressTotal, progressTotalMax)
	{
		if(typeof(internal(this).callbacks.onProgressChange) === "function") {
			// Call callback
			internal(this).callbacks.onProgressChange.call(
					internal(this).callbacks.context,
					this,
					request,
					progressSelf,
					progressSelfMax,
					progressTotal,
					progressTotalMax
			);
		}
	},
	
	
	/**
	 * @internal
	 */
	onRefreshAttempted: function(progress, URI, delay, sameURI)
	{
		if(typeof(internal(this).callbacks.onRefreshAttempted) === "function") {
			// Call callback
			internal(this).callbacks.onRefreshAttempted.call(
					internal(this).callbacks.context,
					this,
					sdk.url.URL(URI.spec),
					delay,
					sameURI
			);
		}
	},
	
	
	/**
	 * @internal
	 */
	onSecurityChange: function(progress, request, stateMask)
	{
		if(typeof(internal(this).callbacks.onSecurityChange) === "function") {
			// Map security state
			var state = null;
			if((stateMask | Ci.nsIWebProgressListener.STATE_IS_BROKEN) === stateMask) {
				state = "broken";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_IS_INSECURE) === stateMask) {
				state = "insecure";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_IS_SECURE) === stateMask) {
				state = "secure";
			}
			
			// Map security strength
			var strength = null;
			if((stateMask | Ci.nsIWebProgressListener.STATE_SECURE_HIGH) === stateMask) {
				state = "high";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_SECURE_MED) === stateMask) {
				state = "med";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_SECURE_LOW) === stateMask) {
				state = "low";
			}
			
			// Call callback
			internal(this).callbacks.onSecurityChange.call(
					internal(this).callbacks.context,
					this,
					request,
					state,
					strength
			);
		}
	},
	
	
	/**
	 * @internal
	 */
	onStateChange: function(progress, request, stateMask, status)
	{
		if(typeof(internal(this).callbacks.onStateChange) === "function") {
			// Map security state
			var transition = null;
			if((stateMask | Ci.nsIWebProgressListener.STATE_START) === stateMask) {
				transition = "start";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_REDIRECTING) === stateMask) {
				transition = "redirecting";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_TRANSFERING) === stateMask) {
				transition = "transfering";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_NEGOTIATING) === stateMask) {
				transition = "negotiating";
			} else if((stateMask | Ci.nsIWebProgressListener.STATE_STOP) === stateMask) {
				transition = "stop";
			}
			
			// Map security strength
			var types = [];
			if((stateMask | Ci.nsIWebProgressListener.STATE_IS_REQUEST) === stateMask) {
				types.push("request");
			}
			if((stateMask | Ci.nsIWebProgressListener.STATE_IS_DOCUMENT) === stateMask) {
				types.push("document");
			}
			if((stateMask | Ci.nsIWebProgressListener.STATE_IS_NETWORK) === stateMask) {
				types.push("network");
			}
			if((stateMask | Ci.nsIWebProgressListener.STATE_IS_WINDOW) === stateMask) {
				types.push("window");
			}
			
			// Call callback
			internal(this).callbacks.onStateChange.call(
					internal(this).callbacks.context,
					this,
					request,
					sdk.url.URL(URI.spec),
					transition,
					types,
					(status === Cr.NS_OK)
			);
		}
	},
	
	
	/**
	 * @internal
	 */
	onStatusChange: function(progress, request, statusCode, message)
	{
		if(typeof(internal(this).callbacks.onStatusChange) === "function") {
			// Map status code
			var status = statusCode;
			if(statusCode === Ci.nsSocketTransport.STATUS_CONNECTING_TO) {
				status = "connecting";
			} else if(statusCode === Ci.nsISocketTransport.STATUS_CONNECTED_TO) {
				status = "connected";
			} else if(statusCode === Ci.nsISocketTransport.STATUS_RECEIVING_FROM) {
				status = "receiving";
			} else if(statusCode === Ci.nsISocketTransport.STATUS_RESOLVING_TO) {
				status = "resolving";
			} else if(statusCode === Ci.nsISocketTransport.STATUS_RESOLVED_TO) {
				status = "resolved";
			} else if(statusCode === Ci.nsISocketTransport.STATUS_SENDING_TO) {
				status = "sending";
			} else if(statusCode === Ci.nsISocketTransport.STATUS_WAITING_FOR) {
				status = "waiting";
			} else if(statusCode === Ci.nsITransport.STATUS_READING) {
				status = "reading";
			} else if(statusCode === Ci.nsITransport.STATUS_WRITING) {
				status = "writing";
			}
			
			// Call callback
			internal(this).callbacks.onStatusChange.call(
					internal(this).callbacks.context,
					this,
					request,
					status,
					message
			);
		}
	},
};


module.exports = {
	TabProgressListener: TabProgressListener
};
