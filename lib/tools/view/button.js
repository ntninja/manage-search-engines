const sdk = {
	ui: {
		button: {
			view: require("sdk/ui/button/view")
		},
		id: require("sdk/ui/id")
	},
	view: {
		core: require("sdk/view/core")
	},
	window: {
		utils: require("sdk/window/utils")
	}
};



const XMLNS_XUL = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";



module.exports = {
	viewFor: function(button_model, window_model=null, get_xbl_image=false)
	{
		var window;
		if(typeof(window_model) === "object" && window_model) {
			window = sdk.view.core.viewFor(window_model);
		} else {
			window = sdk.window.utils.getMostRecentBrowserWindow();
		}
		
		// Unfortunately one cannot pass the relevant window object to `sdk.view.core.viewFor` :-(
		//var button = sdk.view.core.viewFor(buttonModel);
		
		// Lower-level alternative -> Also doesn't work when moving the button onto a different toolbar o.O
		//var button = sdk.ui.button.view.nodeFor(sdk.ui.id.identify(button_model), window);
		
		// Finally something that works (At least we don't have to build the ID ourselved anymore...)
		var button = window.document.getElementById(sdk.ui.id.identify(button_model));
		
		// Retrive the anonymous XBL xul:image node within the toolbar button
		// This is sometimes useful since the xul:image is more easily customizable than the xul:toolbarbutton
		// and sometimes better represents the actual location of the button
		if(get_xbl_image && button.ownerDocument.getAnonymousNodes && button.ownerDocument.getAnonymousNodes(button)) {
			for(var node of button.ownerDocument.getAnonymousNodes(button)) {
				if(node.namespaceURI === XMLNS_XUL && node.localName === "image") {
					button = node;
					break;
				}
			}
		}
		
		return button;
	}
};
