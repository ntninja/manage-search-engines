const {Cc, Ci, Cu, Cr} = require("chrome");

const sdk = {
	tabs: require("sdk/tabs"),
	
	core: {
		heritage: require("sdk/core/heritage")
	},
	event: {
		core:   require("sdk/event/core"),
		target: require("sdk/event/target")
	},
	model: {
		core: require("sdk/model/core")
	},
	system: {
		unload: require("sdk/system/unload")
	},
	ui: {
		id: require("sdk/ui/id")
	},
	util: {
		object: require("sdk/util/object")
	},
	view: {
		core: require("sdk/view/core")
	},
	window: {
		utils: require("sdk/window/utils")
	}
};

const tools = {
	content: {
		tabprogress: require("./content/tab-progress")
	}
};



/**
 * @internal
 */
var EngineTracker = sdk.core.heritage.Class({
	extends: sdk.event.target.EventTarget,
	
	
	
	initialize: function(search_engines) {
		this.search_engines = search_engines;
		
		this.engines = {};
		
		this.onTabOpenHandler  = this.onTabOpen.bind(this);
		this.onTabCloseHandler = this.onTabClose.bind(this);
		
		this.onEngineAddedHandler = this.onEngineAdded.bind(this);
		
		this.onAddSearchHandler = this.onAddSearch.bind(this);
		
		// Watch tabs opening and closing
		sdk.tabs.on("open",  this.onTabOpenHandler);
		sdk.tabs.on("close", this.onTabCloseHandler);
		
		// Watch engines appearing
		this.search_engines.on("engine-added", this.onEngineAddedHandler);
		
		// Register existing tabs
		for(var tab_model of sdk.tabs) {
			this.onTabOpen(tab_model);
		}
		
		// Make sure the clean-up function is called
		sdk.system.unload.ensure(this, "destroy");
	},
	
	
	destroy: function() {
		// Stop watching tabs
		sdk.tabs.removeListener("close", this.onTabCloseHandler);
		sdk.tabs.removeListener("open",  this.onTabOpenHandler);
		
		this.search_engines.removeListener("engine-added", this.onEngineAddedHandler);
		
		// Release all per-tab resources
		for(var tab_model of sdk.tabs) {
			this.onTabClose(tab_model);
		}
	},
	
	
	addEngine: function(tab_model, engine_info) {
		// Connect to IO service
		var nsIOService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		
		var iconURI = null;
		try {
			iconURI = nsIOService.newURI(engine_info.icon, null, null);
		} catch(e) {}
		
		// Convert engine info to nsIBrowserSearch style object and store it in the engine list for this tab
		var engine = {
			alias:       null,
			description: null,
			hidden:      null, // Set by `module.exports.getOfferedEngines()`
			iconURI:     iconURI,
			name:        engine_info.title,
			searchForm:  null,
			type:        Ci.nsISearchEngine.TYPE_OPENSEARCH,
			uri:         engine_info.uri,
			
			addParam: function(name, value, responseType)
			{
				throw Cr.NS_ERROR_FAILURE;
			},
			
			getSubmission: function(data, responseType, purpose)
			{
				return {
					postData: null,
					uri:      "http://example.org/"
				};
			},
			
			supportsResponseType: function(responseType)
			{
				return false;
			}
		};
		this.engines[sdk.ui.id.identify(tab_model)].push(engine);
		
		// Let listeners know about this
		sdk.event.core.emit(this, "engine-added", tab_model, sdk.util.object.extend({}, engine));
	},
	
	
	getEnginesForTabModel: function(tab_model) {
		// Remove any engines from list that have been added to the browser in the meantime
		return this.engines[sdk.ui.id.identify(tab_model)].filter(function(e)
		{
			return !this.search_engines.getEngineByName(e.name);
		}.bind(this));
	},
	
	
	getBrowserForTabModel: function(tab_model) {
		// Get low-level tab and window
		var tab    = sdk.view.core.viewFor(tab_model);
		var window = sdk.view.core.viewFor(tab_model.window);
		
		// Get tab browser instance
		return window.gBrowser.getBrowserForTab(tab);
	},
	
	
	getTabModelForBrowser: function(browser) {
		// Get low-levl tab and window
		var window = browser.ownerDocument.defaultView.window;
		var tab    = window.gBrowser.getTabForBrowser(browser);
		
		// Get tab model
		return sdk.model.core.modelFor(tab);
	},
	
	
	onEngineAdded: function(engine) {
		for(var tab_id in this.engines) {
			if(this.engines.hasOwnProperty(tab_id)) {
				for(var i = (this.engines[tab_id].length - 1); i >= 0; i--) {
					if(this.engines[tab_id][i].uri.spec === engine.uri.spec) {
						// This engine has just been added and isn't on offer anymore
						this.engines[tab_id][i].splice(i, 1);
					}
				}
			}
		}
	},
	
	
	onAddSearch: function(message) {
		// Get the browser's tab model
		var tab_model = this.getTabModelForBrowser(message.target);
		
		var engine = message.data.engine;
		// Check to see whether we've already added an engine with this URI
		if(this.engines[sdk.ui.id.identify(tab_model)].some(function(e) e.uri === engine.href)) {
			return;
		}
		
		// Connect to IO service
		var nsIOService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		
		// Create nsIURI from page url
		var nsURI = nsIOService.newURI(message.data.url, null, null);
		
		// Append the URI and an appropriate title to the browser data.
		// Use documentURIObject in the check for shouldLoadFavIcon so that we
		// do the right thing with about:-style error pages.  Bug 453442
		var iconURL = null;
		if(message.target.ownerDocument.defaultView.window.gBrowser.shouldLoadFavIcon(nsURI)) {
			iconURL = nsURI.prePath + "/favicon.ico";
		}
		
		// Add provided engine to list of known engines
		this.addEngine(tab_model, {
			uri:   engine.href,
			title: engine.title,
			icon:  iconURL
		});
	},
	
	
	onTabOpen: function(tab_model) {
		// Get the tab's browser object
		var browser = this.getBrowserForTabModel(tab_model);
		
		// Add engines storage slot
		this.engines[sdk.ui.id.identify(tab_model)] = [];
		
		// Add reference to browser so that its available during shutdown
		this.engines[sdk.ui.id.identify(tab_model)].browser = browser;
		
		// Add existing engines (only collected while search bar is still visible)
		if(browser.engines) {
			for(var engine of browser.engines) {
				this.addEngine(tab_model, engine);
			}
		}
		if(browser.hiddenEngines) {
			for(var engine of browser.hiddenEngines) {
				this.addEngine(tab_model, engine);
			}
		}
		
		// Listen for tab navigation
		this.engines[sdk.ui.id.identify(tab_model)].tabprogress = new tools.content.tabprogress.TabProgressListener(
				tab_model,
				{
					onLocationChange: this.onTabLocationChange,
					context:          this
				}
		);
		
		// Listen for engine updates
		browser.messageManager.addMessageListener("Link:AddSearch", this.onAddSearchHandler);
	},
	
	
	onTabLocationChange: function(tabprogress, request_model, url_model, flags) {
		// When switching pages clear list of search engines
		this.engines[sdk.ui.id.identify(tabprogress.tab)].splice(
				0, this.engines[sdk.ui.id.identify(tabprogress.tab)].length
		);
	},
	
	
	onTabClose: function(tab_model) {
		// Get the tab's browser object
		var browser = this.engines[sdk.ui.id.identify(tab_model)].browser;
		
		// Stop listening for engine updates
		browser.messageManager.removeMessageListener("Link:AddSearch", this.onAddSearchHandler);
		
		// Stop listening for tab navigation
		this.engines[sdk.ui.id.identify(tab_model)].tabprogress.destroy();
		
		// Delete engine storage slot
		delete this.engines[sdk.ui.id.identify(tab_model)];
	}
});



const SearchEngineInterface = sdk.core.heritage.Class({
	extends: sdk.event.target.EventTarget,
	
	
	
	initialize: function() {
		// Connect to service
		this.service = Cc["@mozilla.org/browser/search-service;1"].getService(Ci.nsIBrowserSearchService);
		
		// Initialize service if it hasn't happened already and hope that its done by the time the first request for
		// data arrives (if its not then the first request will only be delayed a bit...)
		this.service.init();
		
		// Start monitoring for search engines offered by web pages
		this.engine_tracker = new EngineTracker(this);
		
		// Make events about new engines available to consumers
		this.engine_tracker.on("engine-added", function(tab_model, engine) {
			// Delay engine notifications until the search service has been initialized
			// (dramatically reduces the likelyhood for calling search service methods too early)
			this.service.init(function() {
				sdk.event.core.emit(this, "engine-available", tab_model, engine);
			}.bind(this));
		}.bind(this));
	},
	
	
	
	CONST: {
		TYPE_MOZSEARCH:  Ci.nsISearchEngine.TYPE_MOZSEARCH,
		TYPE_SHERLOCK:   Ci.nsISearchEngine.TYPE_SHERLOCK,
		TYPE_OPENSEARCH: Ci.nsISearchEngine.TYPE_OPENSEARCH,
		
		DATA_XML:  Ci.nsISearchEngine.DATA_XML,
		DATA_TEXT: Ci.nsISearchEngine.DATA_TEXT
	},
	
	get currentEngine() {
		return this.service.currentEngine;
	},
	
	set currentEngine(engine) {
		return this.service.currentEngine = engine;
	},
	
	
	/**
	 * Delay the given callback until the search service has completed its initialization
	 */
	waitUntilReady: function(callback) {
		this.service.init(function() {
			callback();
		});
	},
	
	
	addEngine: function(engineURL, dataType, iconURL, confirm, callback) {
		return this.service.addEngine(engineURL, dataType, iconURL, confirm, {
			onError: function(errno) {
				if(typeof(callback) === "function") {
					callback(errno, false);
				}
			}.bind(this),
			 
			onSuccess: function(engine) {
				// Connect to IO service
				var nsIOService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
				
				// Add original engine URL to allow event listener to uniquely identify this engine in the list of
				// offered engines
				engine = sdk.util.object.extend({}, engine, {
					uri: nsIOService.newURI(engineURL, null, null)
				});
				
				// Notify listeners that the engine might have changed
				//XXX: This only works if nsISearchEngineManager.addEngine was called using this function but its the
				//     best we can do
				sdk.event.core.emit(this, "engine-added", engine);
				
				if(typeof(callback) === "function") {
					callback(engine, true);
				}
			}.bind(this)
		});
	},
	
	
	getEngineByName: function(engine_name) {
		return this.service.getEngineByName(engine_name);
	},
	
	
	getVisibleEngines: function() {
		return this.service.getVisibleEngines();
	},
	
	
	getOfferedEngines: function(tab_model=null, get_hidden=false) {
		// Get tab model
		if(typeof(tab_model) !== "object" || !tab_model) {
			tab_model = sdk.tabs.activeTab;
		}
		
		// Get list of engines
		var engines = this.engine_tracker.getEnginesForTabModel(tab_model);
		
		// Update "hidden" flag
		for(var engine of engines) {
			engine.hidden = !!this.getEngineByName(engine.name);
		}
		
		if(get_hidden) {
			return engines;
		} else {
			return engines.filter(function(e) !e.hidden);
		}
	}
});

module.exports = new SearchEngineInterface();
