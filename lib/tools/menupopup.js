"use strict";

const sdk = {
	core: {
		heritage:  require("sdk/core/heritage"),
		namespace: require("sdk/core/namespace")
	},
	event: {
		core:   require("sdk/event/core"),
		target: require("sdk/event/target")
	},
	system: {
		unload: require("sdk/system/unload")
	},
	util: {
		object: require("sdk/util/object")
	},
	view: {
		core: require("sdk/view/core")
	},
	window: {
		utils: require("sdk/window/utils")
	}
};


const XMLNS_XUL = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";

const ITEM_CSS_HIGHLIGHT = "font-weight: bold; box-shadow: inset 0 0 1em 1em rgba(0, 0, 0, 0.1);";



var internal = sdk.core.namespace.ns();

function addEventListener(element, eventname, callback, context)
{
	// Add DOM event listener
	var listener = callback.bind(context);
	element.addEventListener(eventname, listener, false);
	
	// Return clean up function
	return {
		destroy: function()
		{
			element.removeEventListener(eventname, listener, false);
		}
	};
}



var Item = sdk.core.heritage.Class({
	extends: sdk.event.target.EventTarget,
	
	
	get title()
	{
		return internal(this).element.getAttribute("label");
	},
	set title(title)
	{
		internal(this).element.setAttribute("label", title);
	},
	
	
	get icon()
	{
		return internal(this).element.getAttribute("src");
	},
	set icon(icon)
	{
		// Add resolution information to icon URL
		if(icon) {
			var window = sdk.window.utils.getMostRecentBrowserWindow();
			
			var size = Math.round(16 * window.devicePixelRatio);
			if(!icon.includes("#")) {
				icon += "#-moz-resolution=" + size + "," + size;
			}
		}
		internal(this).element.setAttribute("src", icon);
		
		// Update class name
		var classNames = internal(this).element.getAttribute("class").split(" ");
		// Remove internal class names from list
		if(classNames.indexOf("menuitem-iconic") > -1) {
			classNames.splice(classNames.indexOf("menuitem-iconic"), 1);
		}
		if(classNames.indexOf("menuitem-with-favicon") > -1) {
			classNames.splice(classNames.indexOf("menuitem-with-favicon"), 1);
		}
		// Add applicable internal class names
		if(icon) {
			classNames.push("menuitem-iconic");
		}
		if(icon && icon.indexOf("://") > -1) {
			classNames.push("menuitem-with-favicon");
		}
		// Set new class name string
		internal(this).element.setAttribute("class", classNames.join(" "));
	},
	
	
	get highlighted()
	{
		return (internal(this).element.getAttribute("style").indexOf(ITEM_CSS_HIGHLIGHT) > -1);
	},
	set highlighted(highlighted)
	{
		var style = (internal(this).element.getAttribute("style") || "");
		if(highlighted) {
			if(!this.highlighted) {
				internal(this).element.setAttribute("style", (ITEM_CSS_HIGHLIGHT + style));
			}
		} else {
			internal(this).element.setAttribute("style", style.replace(ITEM_CSS_HIGHLIGHT, ""));
		}
	},
	
	
	initialize: function(options)
	{
		if(!(this instanceof Item)) {
			return new Item(options);
		}
		
		internal(this).cleaners = [];
		
		// Create XUL element
		var window   = sdk.window.utils.getMostRecentBrowserWindow();
		var document = window.document;
		internal(this).element = document.createElementNS(XMLNS_XUL, "menuitem");
		
		// Handle DOM events
		internal(this).cleaners.push(addEventListener(internal(this).element, "command", function(event)
		{
			// Trigger event
			sdk.event.core.emit(this, "click", this);
		}, this).destroy);
		
		// Set option properties on local object
		sdk.util.object.safeMerge(this, options);
		
		// Connect callbacks
		if(options && typeof(options.onclick) === "function") {
			this.on("click", options.onclick);
		}
		
		// Handle unload cleanly
		sdk.system.unload.ensure(this, "destroy");
	},
	
	
	destroy: function()
	{
		if(!internal(this).element) {
			return this;
		}
		
		// Call cleaners
		for(var cleaner of internal(this).cleaners) {
			cleaner();
		}
		
		// Destroy all references
		delete internal(this).cleaners;
		delete internal(this).element;
		
		return this;
	}
});

sdk.view.core.getNodeView.define(Item, function(item)
{
	return internal(item).element;
});



var Separator = sdk.core.heritage.Class({
	extends: sdk.event.target.EventTarget,
	
	
	initialize: function(options)
	{
		if(!(this instanceof Separator)) {
			return new Separator(options);
		}
	
		// Create XUL element
		var window   = sdk.window.utils.getMostRecentBrowserWindow();
		var document = window.document;
		internal(this).element = document.createElementNS(XMLNS_XUL, "menuseparator");
	
		// Set option properties on local object
		sdk.util.object.safeMerge(this, options);
		
		// Handle unload cleanly
		sdk.system.unload.ensure(this, "destroy");
	},
	
	
	destroy: function()
	{
		if(!internal(this).element) {
			return this;
		}
		
		// Destroy all references
		delete internal(this).element;
		
		return this;
	}
});

sdk.view.core.getNodeView.define(Separator, function(separator)
{
	return internal(separator).element;
});



var Menu = sdk.core.heritage.Class({
	extends: sdk.event.target.EventTarget,
	
	
	get isShowing()
	{
		return internal(this).is_showing;
	},
	
	get focus()
	{
		return true;
	},
	
	
	initialize: function Menu(options)
	{
		this.content = [];
		
		internal(this).is_showing = false;
		internal(this).cleaners   = [];
		
		// Create XUL element
		var window   = sdk.window.utils.getMostRecentBrowserWindow();
		var document = window.document;
		internal(this).element = document.createElementNS(XMLNS_XUL, "menupopup");
		
		// Handle events
		internal(this).cleaners.push(addEventListener(internal(this).element, "popupshowing", function(event)
		{
			// Set flag
			internal(this).is_showing = true;
			
			// Trigger event
			sdk.event.core.emit(this, "open", this);
		}, this).destroy);
		internal(this).cleaners.push(addEventListener(internal(this).element, "popuphiding", function(event)
		{
			// Set flag
			internal(this).is_showing = false;
			
			// Trigger event
			sdk.event.core.emit(this, "close", this);
		}, this).destroy);
		
		// Set option properties on local object
		sdk.util.object.safeMerge(this, options);
		
		// Connect callbacks
		if(options && typeof(options.onopen) === "function") {
			this.on("open", options.onclick);
		}
		if(options && typeof(options.onclose) === "function") {
			this.on("close", options.onclick);
		}
		
		// Handle unload cleanly
		sdk.system.unload.ensure(this, "destroy");
	},
	
	
	destroy: function()
	{
		if(!internal(this).element) {
			return this;
		}
		
		// Don't leave broken popup open
		if(internal(this).is_showing) {
			internal(this).element.hide();
		}
		
		// Call cleaners
		for(var cleaner of internal(this).cleaners) {
			cleaner();
		}
		
		// Destroy all references
		delete internal(this).cleaners;
		delete internal(this).element;
		
		return this;
	},
	
	
	show: function(options)
	{
		if(!internal(this).element) {
			return this;
		}
		
		// Merge together final options object
		var options = sdk.util.object.extend({}, this, options);
		
		var anchor   = null;
		var window   = sdk.window.utils.getMostRecentBrowserWindow();
		var document = window.document;
		var pos_x    = 0;
		var pos_y    = 0;
		if(sdk.view.core.viewFor(options.position) && sdk.view.core.viewFor(options.position).ownerDocument) {
			// Widget
			anchor   = sdk.view.core.viewFor(options.position);
			window   = anchor.ownerDocument.defaultView.window;
			document = anchor.ownerDocument;
			
			// Detect whether the toolbar button is on the navigation bar or not
			for(var node = anchor; node !== null; node = node.parentNode) {
				if(node.id === "nav-bar") {
					// When the anchor is on the navigation bar try attaching the menu to the XBL image within
					// the toolbar button since its closer to where we actually want the menu in this case
					if(document.getAnonymousNodes && document.getAnonymousNodes(anchor)) {
						for(var node of document.getAnonymousNodes(anchor)) {
							if(node.namespaceURI === XMLNS_XUL && node.localName === "image") {
								anchor = node;
								break;
							}
						}
					}
					break;
				}
			}
		} else {
			pos_x = window.availWidth  / 2;
			pos_y = window.availHeight / 2;
			if(typeof(options.position.left) === "number") {
				pos_x = options.position.left;
			}
			if(typeof(options.position.top) === "number") {
				pos_y = options.position.top;
			}
			//TODO: Process right and bottom
		}
		
			
		
		var element = internal(this).element;
		
		// Attach content items to popup
		for(var item_model of this.content) {
			if(sdk.view.core.viewFor(item_model)) {
				element.appendChild(sdk.view.core.viewFor(item_model));
			}
		}
		
		// Attach popup to document
		document.documentElement.appendChild(element);
		
		// Clean up tasks
		element.addEventListener("popuphidden", function func()
		{
			element.removeEventListener("popuphidden", func, false);
			
			// Detach all content items from popup
			for(var i = (element.childNodes.length - 1); i >= 0; i--) {
				element.removeChild(element.childNodes[i]);
			}
			
			// Detach from document
			document.documentElement.removeChild(element);
		}, false);
		
		// Show popup
		element.openPopup(anchor, "after_start", pos_x, pos_y, false, true, null);
		
		return this;
	},
	
	
	hide: function()
	{
		if(!internal(this).element) {
			return this;
		}
		
		if(internal(this).is_showing) {
			internal(this).element.hidePopup();
		}
	}
});

sdk.view.core.getNodeView.define(Menu, function(menu)
{
	return internal(menu).element;
});



module.exports = {
	Menu:      Menu,
	Separator: Separator,
	Item:      Item
};
