/**
 * Highlight a toolbar button
 *
 * The way this is implemented seems to work on my machine, unless you drag it on to the menubar
 * (makes it look strange). Unfortunately proper integration for this feature on every platform
 * would probably require a GUI designer maintaining stylsheets for each toolbar configuration
 * on each supported platform...
 */
const sdk = {
	windows: require("sdk/windows")
};

const tools = {
	view: {
		button: require("../../view/button")
	}
};



const BUTTON_STYLE = "background-image: -moz-radial-gradient(center 45deg, Highlight 15%, rgba(255,255,255,0) 70%)";


function button_set_style(button_model, window_models, style)
{
	for(var window_model of window_models) {
		// Get XUL node for button model
		var button_node = tools.view.button.viewFor(button_model, window_model, true);
	
		// Set button style
		button_node.setAttribute("style", style);
	
		// Also apply style to all hbox elements within the XUL toolbar button
		var document = button_node.ownerDocument;
		for(var button_subnodes = document.getAnonymousNodes(button_node), i = 0; i < button_subnodes.length; i++)
		{
			if(button_subnodes[i].namespaceURI == "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul"
			&& button_subnodes[i].localName == "hbox") {
				button_subnodes[i].setAttribute("style", style);
			}
		}
	}
}


module.exports = {
	enable: function(button_model, window_models=sdk.windows.browserWindows)
	{
		button_set_style(button_model, window_models, BUTTON_STYLE);
	},
	
	disable: function(button_model, window_models=sdk.windows.browserWindows)
	{
		button_set_style(button_model, window_models, "");
	},
};
