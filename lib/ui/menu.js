const sdk = {
	l10n:        require("sdk/l10n"),
	simpleprefs: require("sdk/simple-prefs"),
	
	util: {
		object: require("sdk/util/object"),
	}
};

const tools = {
	menupopup:     require("../tools/menupopup"),
	searchengines: require("../tools/search-engines"),
};

const m = {
	ui: {
		manager: require("../ui/manager"),
	}
}

const _ = sdk.l10n.get;



function buildEngineMenu()
{
	function add_engines(menu, engines, callback)
	{
		for(var engine of engines) {
			var menuitem = new tools.menupopup.Item({
				title: engine.name,
				icon:  (engine.iconURI ? engine.iconURI.spec : null),
				
				onclick: (function(engine)
				{
					return function(event)
					{
						callback(engine, event);
					};
				})(engine)
			});
			
			// Highlight default engine
			if(engine == tools.searchengines.currentEngine) {
				menuitem.highlighted = true;
			}
			
			menu.content.push(menuitem);
		}
	}
	
	// Create menu
	var menu = new tools.menupopup.Menu();
	
	// Add all currently visible search engines to list
	add_engines(menu, tools.searchengines.getVisibleEngines(), function(engine, event)
	{
		tools.searchengines.currentEngine = engine;
	});
	
	// Add search engines offered by the current webpage to list
	var engines_offered = tools.searchengines.getOfferedEngines()
	if(engines_offered.length > 0) {
		// Add separator
		menu.content.push(new tools.menupopup.Separator());
		
		// Add correct engine text
		engines_offered = engines_offered.map(function(engine)
		{
			return sdk.util.object.extend({}, engine, {
				name: _("Add \"%s\"", engine.name)
			});
		});
		
		// Add list of engines offered by website
		add_engines(menu, engines_offered, function(engine, event)
		{
			tools.searchengines.addEngine(
					engine.uri,
					tools.searchengines.CONST.DATA_XML,
					(engine.iconURI ? engine.iconURI.spec : null),
					sdk.simpleprefs.prefs["display-addengine-dialog"]
			);
		});
	}
	
	// Add the final separator
	menu.content.push(new tools.menupopup.Separator());
	
	// Main actor
	menu.content.push(new tools.menupopup.Item({
		"title": _("Manage search engines..."),
		
		onclick: function(event)
		{
			m.ui.manager.open();
		}
	}));
	
	return menu;
}


function show(properties, callback_done)
{
	// Build search engine menu
	var menu = buildEngineMenu();
	
	// Untoggle toolbar button on close
	if(callback_done) {
		menu.once("close", callback_done);
	}
	
	// Display menu
	menu.show(properties);
}


module.exports = {
	show: show
};