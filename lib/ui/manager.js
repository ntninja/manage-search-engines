const sdk = {
	tabs:   require('sdk/tabs'),
	system: require("sdk/system"),
	
	window: {
		utils: require('sdk/window/utils')
	}
};


module.exports = {
	open: function()
	{
		if(parseInt(sdk.system.version.split(".")[0]) >= 43) {
			// Just show the search engines preferences page (search manager window has been removed)
			sdk.tabs.open("about:preferences#search");
		} else {
			// Enumerate currently opened windows
			let windows = sdk.window.utils.windows();
			
			let searchManager;
			windows.forEach(function(window)
			{
				if(window.document && window.document.documentElement) {
					if(window.document.documentElement.getAttribute("windowtype") == "Browser:SearchManager") {
						searchManager = window;
					}
				}
			});
			
			// Select search manager window if it is already open
			if(searchManager) {
				searchManager.focus();
			
			// Open new search manager if there isn't one open already
			} else {
				sdk.window.utils.openDialog({
					url:  "chrome://browser/content/search/engineManager.xul",
					//url:  "about:preferences#search",
					name: "_blank",
					
					features: "chrome,dialog,modal,centerscreen"
				});
			}
		}
	}
};
